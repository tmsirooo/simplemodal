var video1, video2;

var onYouTubeIframeAPIReady=function () {
    video1 = new YT.Player('video1', {
        height: '360',
        width: '640',
        videoId: 'M7lc1UVf-VE'
    });
    video2 = new YT.Player('video2', {
        height: '360',
        width: '640',
        videoId: 'HvX_6y0BBCo'
    });
}

var modal = (function() {
    var backdrop = document.querySelector('.backdrop');
    var backdropDisplayBlock = function () {
        backdrop.style.display = 'block';
    };
    var backdropDisplayNone = function () {
        backdrop.style.display = 'none';
    };

    var image1 = document.querySelector('.trip-image1');
    var image2 = document.querySelector('.trip-image2');
    var modal1 = document.querySelector('.modal1');
    var modal2 = document.querySelector('.modal2');

    var openModal_1 = function () {
        backdropDisplayBlock()
        modal1.style.display = 'block';
        video1.playVideo();
    }
    var closeModal_1 = function () {
        backdropDisplayNone()
        modal1.style.display = 'none';
        video1.pauseVideo();
    }

    var openModal_2 = function () {
        backdropDisplayBlock()
        modal2.style.display = 'block';
        video2.playVideo();
    }
    var closeModal_2 = function () {
        backdropDisplayNone()
        modal2.style.display = 'none';
        video2.pauseVideo();
    }

    return {
        opne_1:image1.addEventListener('click', openModal_1),
        close_1:backdrop.addEventListener('click', closeModal_1),

        open_2:image2.addEventListener('click', openModal_2),
        close_2:backdrop.addEventListener('click', closeModal_2),
    }
})();
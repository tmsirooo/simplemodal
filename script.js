var modal = (function() {
    var backdrop = document.querySelector('.backdrop');

    var image1 = document.querySelector('.trip-image1');
    var image2 = document.querySelector('.trip-image2');
    var modal1 = document.querySelector('.modal1');
    var modal2 = document.querySelector('.modal2');

    var openModal_1 = function () {
        backdrop.style.display = 'block';
        modal1.style.display = 'block';
    }
    var closeModal_1 = function () {
        // videoControl("pauseVideo");
        backdrop.style.display = 'none';
        modal1.style.display = 'none';
    }

    var openModal_2 = function () {
        backdrop.style.display = 'block';
        modal2.style.display = 'block';
    }
    var closeModal_2 = function () {
        backdrop.style.display = 'none';
        modal2.style.display = 'none';
    }

    return {
        opne_1:image1.addEventListener('click', openModal_1),
        close_1:backdrop.addEventListener('click', closeModal_1),

        open_2:image2.addEventListener('click', openModal_2),
        close_2:backdrop.addEventListener('click', closeModal_2),
    }
})();



